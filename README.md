Dark Arduino Theme 2.1 for Arduino 1.9.0-beta
Theme based on: https://github.com/jeffThompson/DarkArduinoTheme

#Installation Instructions
backup folder "theme" in "arduino/lib/" and copy "theme" folder from github
to "arduino/lib/".